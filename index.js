let num1 = +prompt("Please enter a number.");
let num2 = +prompt("Please enter another number");

function eval(n1, n2) {
	const total = n1 + n2;
	if (total < 10) {
		console.warn(`Sum is ${total}`);
		return;
	}

	alert(`Total is ${total}`);
	if (total < 20) {
		alert(`Difference is ${n1 - n2}`);
	} else if (total < 30) {
		alert(`Product is ${n1 * n2}.`);
	} else {
		alert(`Quotient is ${n1 / n2}.`);
	}
}

eval(num1, num2);

let user = prompt("Enter name");
let age = prompt("Enter age");

function greet(user, age) {
	user && age
		? alert(`Hello ${user}, you are ${age} years old.`)
		: alert(`Are you a time traveler?`);
}

function isLegalAge(age) {
	age < 18
		? alert("You are not allowed here.")
		: alert("You are of legal age.");

	switch (+age) {
		case 18:
			alert("You are allowed to party.");
			break;
		case 21:
			alert("You are now part of the adult society");
			break;
		case 65:
			alert("We thank you for your contribution to society.");
			break;
		default:
			alert("Are you sure you're not an alien?");
	}
}

greet(user, age);

try {
	isLegalAgee(age);
} catch (error) {
	console.warn(error.message);
} finally {
	isLegalAge(age);
}
